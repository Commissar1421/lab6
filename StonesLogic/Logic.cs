﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StonesLibrary;

namespace StonesLogic
{
    public static class StonesPrinter
    {
        public static void PrintWeapon(AbstractStone stone)
        {
            Console.WriteLine("Название: " + stone.Name + ", стоимость: " + stone.Price.ToString() + " рублей, цвет: " + stone.Color + ", вес: " + stone.Weight.ToString() + " карат.");
        }

        public static void PrintNecklace(Necklace necklace)
        {
            Console.WriteLine("Камни входящие в ожерелье: \n");
            foreach (AbstractStone stone in necklace.getStonesSet())
            {
                PrintWeapon(stone);
            }
            Console.WriteLine("Стоимость ожерелья составляет " + StonesCalculator.GetTotalPrice(necklace).ToString() + " рублей.");
            Console.WriteLine("Вес камней ожерелья составляет " + StonesCalculator.GetTotalWeight(necklace).ToString() + " карат.");
        }
    }

    public static class StonesCalculator
    {
        public static double GetTotalPrice(Necklace necklace)
        {
            double totalPrice = 0;
            foreach (AbstractStone stone in necklace.getStonesSet())
            {
                totalPrice += stone.Price;
            }
            return totalPrice;
        }
        public static int GetTotalWeight(Necklace necklace)
        {
            int totalWeight = 0;
            foreach (AbstractStone stone in necklace.getStonesSet())
            {
                totalWeight += stone.Weight;
            }
            return totalWeight;
        }
    }

    public class NecklaceFactory
    {
        public static Necklace CreateNecklace()
        {
            Necklace necklace = new Necklace();

            Diamond century = new Diamond
            {
                Name = "Century",
                Price = 266000,
                Weight = 3,
                Color = "Бесцветный",
                TransparencyDegree = 1.0,
                Hardness = 10
            };

            Emerald teodora = new Emerald
            {
                Name = "Teodora",
                Price = 68000,
                Weight = 5,
                Color = "Зеленый",
                TransparencyDegree = 0.67,
                Saturation = 0.87
            };

            Nephritis hetian = new Nephritis
            {
                Name = "Hetian",
                Price = 33000,
                Weight = 4,
                Color = "Белый",
                Density = 3.0,
                Origin = "Китай"
            };

            necklace.AddStone(century);
            necklace.AddStone(teodora);
            necklace.AddStone(hetian);
            return necklace;
        }
        public static AbstractStone CreateStone(string newClass, string newName, string newPrice, string newWeight, string newColor)
        {
            AbstractStone stone = newClass switch
            {
                "Diamond" => new Diamond(),
                "Ruby" => new Ruby(),
                "Emerald" => new Emerald(),
                "Nephritis" => new Nephritis(),
                "Obsidian" => new Obsidian(),
                _ => new Diamond(),
            };
            stone.Name = newName;
            try
            {
                stone.Price = int.Parse(newPrice);
                stone.Weight = int.Parse(newWeight);
            }
            catch
            {
                stone.Price = 0;
                stone.Weight = 0;
            }
            stone.Color = newColor;
            return stone;
        }
    }
}
