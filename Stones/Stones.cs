﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StonesLibrary
{
    public abstract class AbstractStone
    {
        private string _name;
        private int _price;
        private int _weight;
        private string _color;
        public abstract string getClass();
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
    }
    public abstract class AbstractPreciousStone: AbstractStone
    {
        private double _trancparencyDegree;
        public double TransparencyDegree
        {
            get { return _trancparencyDegree; }
            set { _trancparencyDegree = value; }
        }
    }
    public class Diamond: AbstractPreciousStone
    {
        private int _hardness;
        public int Hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }
        public override string getClass()
        {
            return ("Diamond");
        }

    }
    public class Ruby : AbstractPreciousStone
    {
        private string _form;
        public string Form
        {
            get { return _form; }
            set { _form = value; }
        }
        public override string getClass()
        {
            return ("Ruby");
        }
    }
    public class Emerald : AbstractPreciousStone
    {
        private double _saturation;
        public double Saturation
        {
            get { return _saturation; }
            set { _saturation = value; }
        }
        public override string getClass()
        {
            return ("Emerald");
        }
    }
    public abstract class AbstractSemipreciousStone : AbstractStone
    {
        private double _density;
        public double Density
        {
            get { return _density; }
            set { _density = value; }
        }
    }
    public class Nephritis : AbstractSemipreciousStone
    {
        private string _origin;
        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public override string getClass()
        {
            return ("Nephritis");
        }
    }
    public class Obsidian : AbstractSemipreciousStone
    {
        private double _waterContent;
        public double WaterContent
        {
            get { return _waterContent; }
            set { _waterContent = value; }
        }
        public override string getClass()
        {
            return ("Obsidian");
        }
    }
    public class Necklace
    {
        private List<AbstractStone> _stonesSet = new List<AbstractStone>();

        public void AddStone(AbstractStone newStone)
        {
            _stonesSet.Add(newStone);
        }

        public List<AbstractStone> getStonesSet()
        {
            return _stonesSet;
        }
    }
}
