﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StonesLibrary;
using StonesLogic;

namespace StonesDesktop
{
    public partial class MainForm : Form
    {
        private Necklace necklace;
        private void Refill()
        {
            listBox1.Items.Clear();
            foreach (AbstractStone stone in necklace.getStonesSet())
            {
                listBox1.Items.Add(stone.Name + " - " + stone.Weight.ToString() + " ct - " + stone.Price.ToString() + " Rub");
            }
            TotalPriceLabel.Text = "Total price: " + StonesCalculator.GetTotalPrice(necklace).ToString() + " Rub";
            TotalWeightLabel.Text = "Total weight: " + StonesCalculator.GetTotalWeight(necklace).ToString() + " ct";
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            necklace = NecklaceFactory.CreateNecklace();
            Refill();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            if (addForm.result == DialogResult.OK)
            {
                AbstractStone stone = NecklaceFactory.CreateStone(addForm.newClass, addForm.newName, addForm.newPrice, addForm.newWeight, addForm.newColor);
                necklace.AddStone(stone);
                Refill();
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                AddForm addForm = new AddForm(necklace.getStonesSet()[listBox1.SelectedIndex]);
                addForm.ShowDialog();
                if (addForm.result == DialogResult.OK)
                {
                    AbstractStone stone = NecklaceFactory.CreateStone(addForm.newClass, addForm.newName, addForm.newPrice, addForm.newWeight, addForm.newColor);
                    necklace.getStonesSet()[listBox1.SelectedIndex] = stone;
                    Refill();
                }

            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                necklace.getStonesSet().RemoveAt(listBox1.SelectedIndex);
                Refill();
            }
        }

        private void TotalPriceLabel_Click(object sender, EventArgs e)
        {

        }

        private void TotalWeightLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
