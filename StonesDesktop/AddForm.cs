﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StonesLibrary;
using StonesLogic;

namespace StonesDesktop
{
    public partial class AddForm : Form
    {
        public string newClass;
        public string newName;
        public string newPrice;
        public string newWeight;
        public string newColor;
        public DialogResult result;
        public AddForm()
        {
            InitializeComponent();
            ClassComboBox.Items.AddRange(new string[] {"Diamond",
                        "Ruby",
                        "Emerald",
                        "Nephritis",
                        "Obsidian"});
        }

        public AddForm(AbstractStone stone): this()
        {

            ClassComboBox.SelectedItem = stone.getClass();
            NameTextBox.Text = stone.Name;
            PriceTextBox.Text = stone.Price.ToString();
            WeightTextBox.Text = stone.Weight.ToString();
            ColorTextBox.Text = stone.Color;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ClassComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void PriceTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void WeightTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if ((ClassComboBox.SelectedItem != null) && (NameTextBox.Text != "") && (PriceTextBox.Text != "") && (WeightTextBox.Text != "") && (ColorTextBox.Text != ""))
            {
                newClass = ClassComboBox.SelectedItem.ToString();
                newName = NameTextBox.Text;
                newPrice = PriceTextBox.Text;
                newWeight = WeightTextBox.Text;
                newColor = ColorTextBox.Text;
                result = DialogResult.OK;
                Close();
            }
        }

        private void CanselButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.Cancel;
            Close();
        }
    }
}
